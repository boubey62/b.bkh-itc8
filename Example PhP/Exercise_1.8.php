<?php
    $name="Bou Bi";
    $color="Blue";


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 1.8</title>
</head>
<body>
    <h2>What's your name and your favorite color?</h2>
    <h1>My name is 
        <span style="color:<?php echo $color; ?>">
        <?php echo $name;?>
        </span>
        and my favorite color is 
        <span style="color:<?php echo $color; ?>">
        <?php echo $color; ?>
    
        </span>
    </h1>
    
</body>
</html>